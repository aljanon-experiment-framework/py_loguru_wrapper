#!/usr/bin/env python

# py_loguru_wrapper
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup

setup(
    name='py_loguru_wrapper',
    version='0.1',
    description='Loguru wrapper with convenient default file loggers.',
    url='https://gitlab.inria.fr/aljanon/py_loguru_wrapper',
    author='Alexis Janon',
    author_email='alexis.janon@inria.fr',
    packages=['py_loguru_wrapper'],
    install_requires=['loguru'],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ]
)
