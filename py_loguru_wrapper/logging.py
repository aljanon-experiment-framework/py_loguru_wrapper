#!/usr/bin/python3

# py_loguru_wrapper
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from dataclasses import dataclass
from pathlib import Path
from typing import IO, Callable, Optional, Union

from loguru import logger


@dataclass
class LoggerOptions(object):
    sink: Union[Path, IO[str]]
    format: str = (
        "[<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green>] "
        "{{<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan>}} "
        "<level>{level:"
        "<8}</level>: {message} "
    )
    level: Union[str, int] = "TRACE"
    serialize: bool = False
    enqueue: bool = False
    backtrace: bool = False
    filter: Optional[Union[Callable, str]] = None


@dataclass
class GlobalFileLogger(LoggerOptions):
    compression: Optional[str] = "xz"
    delay: bool = False


@dataclass
class TTYLoggerOptions(LoggerOptions):
    sink: IO[str] = sys.stderr
    level: str = "INFO"


def setup_global_logger(wd: Path) -> None:
    logger.configure(
        handlers=[
            vars(TTYLoggerOptions()),
            vars(
                GlobalFileLogger(
                    sink=wd / "TRACE.log", level="TRACE", serialize=True, backtrace=True
                )
            ),
            vars(GlobalFileLogger(sink=wd / "DEBUG.log", level="DEBUG")),
            vars(GlobalFileLogger(sink=wd / "ERROR.log", level="ERROR", delay=True)),
        ],
        levels=[
            {"name": "DEBUG", "color": "<bold>"},
            {"name": "INFO", "color": "<bold><blue>"},
            {"name": "CRITICAL", "color": "<bold><magenta>"},
        ],
    )
    logger.info(f"Global logs dir: {wd}")


def teardown_global_logger() -> None:
    logger.configure(
        handlers=[vars(TTYLoggerOptions())],
        levels=[
            {"name": "DEBUG", "color": "<bold>"},
            {"name": "INFO", "color": "<bold><blue>"},
            {"name": "CRITICAL", "color": "<bold><magenta>"},
        ],
    )
