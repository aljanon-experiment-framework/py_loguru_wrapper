# py_loguru_wrapper

This repository contains a simple wrapper around the Python
[loguru](https://github.com/Delgan/loguru/) package and provides useful
configuration options.

## Usage

Should be used as a python module. Can be installed in a Pipfile with
```toml
py_loguru_wrapper = { git = 'https://gitlab.inria.fr/aljanon-experiment-framework/py_loguru_wrapper.git', editable = true }
```

Provides three dataclasses:

- `LoggerOptions`: Mainly changes the default format to an (arguably) better one.
  The log level defaults to the lowest (`TRACE`).
- `GlobalFileLogger`: Compressed file logging (defaults to `xz`). The sink
  parameter with a path to the output file must be given to the constructor.
- `TTYLoggerOptions`: For outputting to the console. The default log level is
  `INFO`.

These classes can be subclassed as needed. For instance, let us assume that
you subclassed `GlobalFileLogger` in `SpecificFileLogger`. This new class
may be added to the list of handlers with:
```python
loguru.add(vars(SpecificFileLogger()))
```
Alternatively, you may also reconfigure all handlers, e.g.:
```python
loguru.configure(handlers=[
    vars(SpecificFileLogger())
])
```

Two functions are also provided:

- `setup_global_logger`: Creates a default TTYLogger and three FileLoggers,
  one for each `TRACE`, `DEBUG`, and `ERROR` level. They create their log
  files at the directory given as parameter to the function. The `ERROR.log`
  file is created at the first error level log. `DEBUG.log` and `TRACE.log`
  are always created, but `TRACE.log` serializes all logs as json.
- `teardown_global_logger`: Reconfigures loguru to only have the TTY Logger.
  This is useful at the end of the execution, before archiving the other log
  files (as is done in
  [setup_experiment](https://gitlab.inria.fr/aljanon-experiment-framework/setup_experiment) and
  [teardown_experiment](https://gitlab.inria.fr/aljanon-experiment-framework/teardown_experiment)).
